import 'package:fastocloud_dart_models/src/models/device.dart';

class CheckPointInfo extends DeviceLoginInfo {
  static const _ROUTE_FIELD = 'route';

  final String route;

  CheckPointInfo(
      {required this.route,
      required String ip,
      required int timestamp,
      required String location,
      required String city,
      required String country})
      : super(ip: ip, timestamp: timestamp, location: location, city: city, country: country);

  @override
  CheckPointInfo copy() {
    return CheckPointInfo(
        route: route,
        ip: ip,
        timestamp: timestamp,
        location: location,
        city: city,
        country: country);
  }

  factory CheckPointInfo.fromJson(Map<String, dynamic> json) {
    final dev = DeviceLoginInfo.fromJson(json);
    return CheckPointInfo(
        route: json[_ROUTE_FIELD],
        ip: dev.ip,
        timestamp: dev.timestamp,
        location: dev.location,
        city: dev.city,
        country: dev.country);
  }

  @override
  Map<String, dynamic> toJson() {
    final base = super.toJson();
    base[_ROUTE_FIELD] = route;
    return base;
  }
}
