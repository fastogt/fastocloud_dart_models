import 'package:fastotv_dart/commands_info.dart';
import 'package:fastotv_dart/utils.dart';

class DeviceStatus {
  static const int _NOT_ACTIVE_CONSTANT = 0;
  static const int _ACTIVE_CONSTANT = 1;
  static const int _BANNED_CONSTANT = 2;

  final int _value;

  const DeviceStatus._(this._value);

  int compareTo(DeviceStatus status) {
    return _value.compareTo(status._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NOT_ACTIVE_CONSTANT) {
      return 'NOT ACTIVE';
    } else if (_value == _ACTIVE_CONSTANT) {
      return 'ACTIVE';
    }

    assert(_value == _BANNED_CONSTANT);
    return 'BANNED';
  }

  factory DeviceStatus.fromInt(int type) {
    if (type == _NOT_ACTIVE_CONSTANT) {
      return DeviceStatus.NOT_ACTIVE;
    } else if (type == _ACTIVE_CONSTANT) {
      return DeviceStatus.ACTIVE;
    } else if (type == _BANNED_CONSTANT) {
      return DeviceStatus.BANNED;
    }

    throw 'Unknown device type: $type';
  }

  static List<DeviceStatus> get values => [NOT_ACTIVE, ACTIVE, BANNED];

  static const DeviceStatus NOT_ACTIVE = DeviceStatus._(_NOT_ACTIVE_CONSTANT);
  static const DeviceStatus ACTIVE = DeviceStatus._(_ACTIVE_CONSTANT);
  static const DeviceStatus BANNED = DeviceStatus._(_BANNED_CONSTANT);
}

class Device {
  static const _ID_FIELD = 'id';
  static const _NAME_FIELD = 'name';
  static const _STATUS_FIELD = 'status';
  static const _CREATED_DATE_FIELD = 'created_date';

  static const MIN_NAME_LENGTH = 1;
  static const MAX_NAME_LENGTH = 32;

  final String? id;
  final UtcTimeMsec? createdDate;

  String name;
  DeviceStatus status;

  Device({this.id, required this.name, required this.status, this.createdDate});

  Device.createDefault()
      : id = null,
        name = 'Test',
        status = DeviceStatus.NOT_ACTIVE,
        createdDate = null;

  Device copy() {
    return Device(id: id, name: name, status: status, createdDate: createdDate);
  }

  bool isValid() {
    return name.length >= MIN_NAME_LENGTH && name.length < MAX_NAME_LENGTH && createdDate != 0;
  }

  factory Device.fromJson(Map<String, dynamic> json) {
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    UtcTimeMsec? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    return Device(
        id: id,
        name: json[_NAME_FIELD],
        status: DeviceStatus.fromInt(json[_STATUS_FIELD]),
        createdDate: createdDate);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_NAME_FIELD: name, _STATUS_FIELD: status.toInt()};
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    return result;
  }
}

class DeviceLoginInfo {
  static const _IP_FIELD = 'ip';
  static const _TIMESTAMP_FIELD = 'timestamp';
  static const _LOC_FIELD = 'loc';
  static const _CITY_FIELD = 'city';
  static const _COUNTRY_FIELD = 'country';

  final String ip;
  final int timestamp;
  final String location;
  final String city;
  final String country;

  DeviceLoginInfo(
      {required this.ip,
      required this.timestamp,
      required this.location,
      required this.city,
      required this.country});

  DeviceLoginInfo.createUnknownLocation({required this.timestamp})
      : ip = "0.0.0.0",
        location = "0.0,0.0",
        city = "Unknown",
        country = "UN";

  DeviceLoginInfo copy() {
    return DeviceLoginInfo(
        ip: ip, timestamp: timestamp, location: location, city: city, country: country);
  }

  factory DeviceLoginInfo.fromJson(Map<String, dynamic> json) {
    return DeviceLoginInfo(
        ip: json[_IP_FIELD],
        timestamp: json[_TIMESTAMP_FIELD],
        location: json[_LOC_FIELD],
        city: json[_CITY_FIELD],
        country: json[_COUNTRY_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {
      _IP_FIELD: ip,
      _TIMESTAMP_FIELD: timestamp,
      _LOC_FIELD: location,
      _CITY_FIELD: city,
      _COUNTRY_FIELD: country
    };
  }
}

class DeviceLoginFront extends Device {
  static const _OS_FIELD = 'os';
  static const _PROJECT_FIELD = 'project';
  static const _CPU_BRAND_FIELD = 'cpu_brand';
  static const _LOGINS_FIELD = 'logins';
  static const _ACTIVE_DURATION = 'active_duration';
  static const _LAST_ACTIVE = 'last_active';

  final OperationSystemInfo? deviceOS;
  final Project? deviceProject;
  final String? deviceBrand;
  final List<DeviceLoginInfo>? logins;
  final int? activeDuration; // millisec
  final int? lastActive; // millisec

  int get loginsCount {
    if (logins == null) {
      return 0;
    }

    return logins!.length;
  }

  DeviceLoginFront(
      {String? id,
      required String name,
      required DeviceStatus status,
      UtcTimeMsec? createdDate,
      this.deviceOS,
      this.deviceBrand,
      this.deviceProject,
      this.logins,
      this.activeDuration,
      this.lastActive})
      : super(id: id, name: name, status: status, createdDate: createdDate);

  @override
  DeviceLoginFront copy() {
    return DeviceLoginFront(
        id: id,
        name: name,
        status: status,
        createdDate: createdDate,
        deviceOS: deviceOS,
        deviceBrand: deviceBrand,
        deviceProject: deviceProject,
        logins: logins,
        activeDuration: activeDuration,
        lastActive: lastActive);
  }

  factory DeviceLoginFront.fromJson(Map<String, dynamic> json) {
    final device = Device.fromJson(json);
    OperationSystemInfo? deviceOS;
    Project? deviceProject;
    String? deviceBrand;
    List<DeviceLoginInfo>? logins;
    int? activeDuration;
    int? lastActive;
    if (json.containsKey(_OS_FIELD)) {
      deviceOS = OperationSystemInfo.fromJson(json[_OS_FIELD]);
    }
    if (json.containsKey(_PROJECT_FIELD)) {
      deviceProject = Project.fromJson(json[_PROJECT_FIELD]);
    }
    if (json.containsKey(_CPU_BRAND_FIELD)) {
      deviceBrand = json[_CPU_BRAND_FIELD];
    }
    if (json.containsKey(_LOGINS_FIELD)) {
      final List<DeviceLoginInfo> res = [];
      json[_LOGINS_FIELD].forEach((b) {
        res.add(DeviceLoginInfo.fromJson(b));
      });
      logins = res;
    }
    if (json.containsKey(_ACTIVE_DURATION)) {
      activeDuration = json[_ACTIVE_DURATION];
    }
    if (json.containsKey(_LAST_ACTIVE)) {
      lastActive = json[_LAST_ACTIVE];
    }
    return DeviceLoginFront(
        id: device.id,
        name: device.name,
        status: device.status,
        createdDate: device.createdDate,
        deviceOS: deviceOS,
        deviceProject: deviceProject,
        deviceBrand: deviceBrand,
        logins: logins,
        activeDuration: activeDuration,
        lastActive: lastActive);
  }
}

class SubscriberDevice {
  static const _ID_FIELD = 'sid';

  static const _DEVICE_ID_FIELD = 'device_id';
  static const _DEVICE_OS_FIELD = 'device_os';
  static const _DEVICE_NAME_FIELD = 'device_name';

  final String id; // not changed

  final String deviceId;
  final String deviceName;
  final PlatformType deviceOS;

  SubscriberDevice(
      {required this.id, required this.deviceId, required this.deviceName, required this.deviceOS});

  factory SubscriberDevice.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final os = json[_DEVICE_OS_FIELD];
    return SubscriberDevice(
        id: id,
        deviceId: json[_DEVICE_ID_FIELD],
        deviceName: json[_DEVICE_NAME_FIELD],
        deviceOS: PlatformType.fromString(os));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      _ID_FIELD: id,
      _DEVICE_ID_FIELD: deviceId,
      _DEVICE_NAME_FIELD: deviceName,
      _DEVICE_OS_FIELD: deviceOS.toString()
    };
    return result;
  }
}

class DeviceStat extends SubscriberDevice {
  static const _ACTIVE_DURATION_FIELD = 'active_duration';
  static const _LOGINS_COUNT_FIELD = 'logins_count';
  static const _ACTIVATION_PLACE_FIELD = 'activation_place';
  static const _LAST_PLACE = 'last_place';

  final int loginsCount;
  final int? activeDuration;
  final DeviceLoginInfo? activationPlace;
  final DeviceLoginInfo? lastPlace;

  DeviceStat(
      {required String id,
      required String deviceId,
      required String deviceName,
      required PlatformType deviceOS,
      this.activeDuration,
      required this.loginsCount,
      this.activationPlace,
      this.lastPlace})
      : super(id: id, deviceId: deviceId, deviceName: deviceName, deviceOS: deviceOS);

  factory DeviceStat.fromJson(Map<String, dynamic> json) {
    final base = SubscriberDevice.fromJson(json);
    int? activeDuration;
    if (json.containsKey(_ACTIVE_DURATION_FIELD)) {
      activeDuration = json[_ACTIVE_DURATION_FIELD];
    }
    DeviceLoginInfo? activation;
    if (json.containsKey(_ACTIVATION_PLACE_FIELD)) {
      activation = DeviceLoginInfo.fromJson(json[_ACTIVATION_PLACE_FIELD]);
    }
    DeviceLoginInfo? last;
    if (json.containsKey(_LAST_PLACE)) {
      last = DeviceLoginInfo.fromJson(json[_LAST_PLACE]);
    }
    return DeviceStat(
        id: base.id,
        deviceId: base.deviceId,
        deviceName: base.deviceName,
        deviceOS: base.deviceOS,
        activeDuration: activeDuration,
        loginsCount: json[_LOGINS_COUNT_FIELD],
        activationPlace: activation,
        lastPlace: last);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_LOGINS_COUNT_FIELD: loginsCount};
    if (activeDuration != null) {
      result[_ACTIVE_DURATION_FIELD] = activeDuration;
    }
    if (activationPlace != null) {
      result[_ACTIVATION_PLACE_FIELD] = activationPlace!.toJson();
    }
    if (lastPlace != null) {
      result[_LAST_PLACE] = lastPlace!.toJson();
    }
    return result;
  }
}
