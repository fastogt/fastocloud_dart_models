import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:fastotv_dart/utils.dart';

class PaymentType {
  static const int _STRIPE = 0;
  static const int _PAYPAL = 1;

  final int _value;

  const PaymentType._(this._value);

  int compareTo(PaymentType other) {
    return _value.compareTo(other._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _STRIPE) {
      return 'Stripe';
    }
    assert(_value == _PAYPAL);
    return 'Paypal';
  }

  factory PaymentType.fromInt(int type) {
    if (type == _STRIPE) {
      return PaymentType.STRIPE;
    } else if (type == _PAYPAL) {
      return PaymentType.PAYPAL;
    }

    throw 'Unknown payment type: $type';
  }

  static List<PaymentType> get values => [STRIPE, PAYPAL];

  static const PaymentType STRIPE = PaymentType._(_STRIPE);
  static const PaymentType PAYPAL = PaymentType._(_PAYPAL);
}

abstract class IPayment {
  static const TYPE_FIELD = 'type';

  final PaymentType type;

  IPayment({required this.type});

  IPayment copy();

  factory IPayment.fromJson(Map<String, dynamic> json) {
    final type = PaymentType.fromInt(json[TYPE_FIELD]);

    if (type == PaymentType.STRIPE) {
      return PaymentStripe.fromJson(json);
    } else if (type == PaymentType.PAYPAL) {
      return PaymentPaypal.fromJson(json);
    }

    throw 'Unknown payment type: $type';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {TYPE_FIELD: type.toInt()};
    return result;
  }
}

class PaymentStripe extends IPayment {
  static const STRIPE_FIELD = 'stripe';

  PaymentStripeData stripe;

  PaymentStripe({required this.stripe}) : super(type: PaymentType.STRIPE);

  PaymentStripe.createDefault()
      : stripe = PaymentStripeData.createDefault(),
        super(type: PaymentType.STRIPE);

  @override
  PaymentStripe copy() {
    return PaymentStripe(stripe: stripe.copy());
  }

  factory PaymentStripe.fromJson(Map<String, dynamic> json) {
    return PaymentStripe(stripe: PaymentStripeData.fromJson(json[STRIPE_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[STRIPE_FIELD] = stripe.toJson();
    return result;
  }
}

class PaymentStripeData {
  static const PUB_KEY_FIELD = 'pub_key';
  static const SECRET_KEY_FIELD = 'secret_key';

  String pubKey;
  String secretKey;

  PaymentStripeData({required this.pubKey, required this.secretKey});

  PaymentStripeData.createDefault()
      : pubKey = 'SOME_PUB_KEY',
        secretKey = 'SOME_SECRET_KEY';

  PaymentStripeData copy() {
    return PaymentStripeData(pubKey: pubKey, secretKey: secretKey);
  }

  factory PaymentStripeData.fromJson(Map<String, dynamic> json) {
    return PaymentStripeData(pubKey: json[PUB_KEY_FIELD], secretKey: json[SECRET_KEY_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[PUB_KEY_FIELD] = pubKey;
    result[SECRET_KEY_FIELD] = secretKey;
    return result;
  }
}

class PaymentPaypal extends IPayment {
  static const PAYPAL_FIELD = 'paypal';

  PaymentPaypalData paypal;

  PaymentPaypal({required this.paypal}) : super(type: PaymentType.PAYPAL);

  PaymentPaypal.createDefault()
      : paypal = PaymentPaypalData.createDefault(),
        super(type: PaymentType.PAYPAL);

  @override
  PaymentPaypal copy() {
    return PaymentPaypal(paypal: paypal.copy());
  }

  factory PaymentPaypal.fromJson(Map<String, dynamic> json) {
    return PaymentPaypal(paypal: PaymentPaypalData.fromJson(json[PAYPAL_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[PAYPAL_FIELD] = paypal.toJson();
    return result;
  }
}

class PaymentPaypalData {
  static const CLIENT_ID_FIELD = 'client_id';
  static const CLIENT_SECRET_FIELD = 'client_secret';
  static const MODE_FIELD = 'mode';

  String clientId;
  String clientSecret;
  PaypalMode mode;

  PaymentPaypalData({required this.clientId, required this.clientSecret, required this.mode});

  PaymentPaypalData.createDefault()
      : clientId = 'SOME_CLIENT_ID',
        clientSecret = 'SOME_CLIENT_SECRET',
        mode = PaypalMode.SANDBOX;

  PaymentPaypalData copy() {
    return PaymentPaypalData(clientId: clientId, clientSecret: clientSecret, mode: mode);
  }

  factory PaymentPaypalData.fromJson(Map<String, dynamic> json) {
    return PaymentPaypalData(
        clientId: json[CLIENT_ID_FIELD],
        clientSecret: json[CLIENT_SECRET_FIELD],
        mode: PaypalMode.fromInt(json[MODE_FIELD]));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[CLIENT_ID_FIELD] = clientId;
    result[CLIENT_SECRET_FIELD] = clientSecret;
    result[MODE_FIELD] = mode.toInt();
    return result;
  }
}

class PaypalMode {
  static const int _SANDBOX = 0;
  static const int _LIVE = 1;

  final int _value;

  const PaypalMode._(this._value);

  int compareTo(PaypalMode other) {
    return _value.compareTo(other._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _SANDBOX) {
      return 'Sandbox';
    }

    assert(_value == _LIVE);
    return 'Live';
  }

  factory PaypalMode.fromInt(int type) {
    if (type == _SANDBOX) {
      return PaypalMode.SANDBOX;
    } else if (type == _LIVE) {
      return PaypalMode.LIVE;
    }

    throw 'Unknown paypal mode type: $type';
  }

  static List<PaypalMode> get values => [SANDBOX, LIVE];

  static const PaypalMode SANDBOX = PaypalMode._(_SANDBOX);
  static const PaypalMode LIVE = PaypalMode._(_LIVE);
}

class GoServer extends PackagePublic {
  static const _HOST_FIELD = "host";
  static const _VISIBLE_FIELD = "visible";
  static const _AVAILABLE_ON_SIGNUP_FIELD = 'available_on_signup';
  static const _CREATED_DATE_FIELD = 'created_date';
  static const _LOCATION_FIELD = 'location';

  static const _LOCAL_FIELD = 'local';
  static const _VERSION_FIELD = 'version';

  static const String DEFAULT_SERVER_NAME = 'Package';

  WSServer host;
  bool visible;
  bool availableOnSignup;
  bool local;

  LatLng? location;
  String? version;

  final UtcTimeMsec? createdDate;

  GoServer(
      {required String? id,
      this.createdDate,
      this.location,
      this.version,
      required String name,
      required this.host,
      required this.local,
      required PricePack? price,
      required String description,
      required this.visible,
      required this.availableOnSignup,
      required String backgroundUrl})
      : super(
            id: id,
            name: name,
            backgroundUrl: backgroundUrl,
            price: price,
            description: description);

  GoServer.createDefault(String backgroundUrl, WSServer ws, {bool isLocal = false})
      : createdDate = null,
        location = null,
        version = null,
        host = ws,
        local = isLocal,
        visible = true,
        availableOnSignup = true,
        super(
            id: null,
            name: DEFAULT_SERVER_NAME,
            backgroundUrl: backgroundUrl,
            price: null,
            description: 'This is awesome package!');

  GoServer copy() {
    return GoServer(
        id: id,
        name: name,
        location: location,
        version: version,
        createdDate: createdDate,
        host: host.copy(),
        local: local,
        visible: visible,
        availableOnSignup: availableOnSignup,
        backgroundUrl: backgroundUrl,
        description: description,
        price: price);
  }

  bool isValid() {
    final req = name.isValidServiceName() && host.isValid();
    return req;
  }

  factory GoServer.fromJson(Map<String, dynamic> json) {
    final PackagePublic base = PackagePublic.fromJson(json);
    UtcTimeMsec? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    LatLng? location;
    if (json.containsKey(_LOCATION_FIELD)) {
      location = LatLng.fromJson(json[_LOCATION_FIELD]);
    }
    String? version;
    if (json.containsKey(_VERSION_FIELD)) {
      version = json[_VERSION_FIELD];
    }
    return GoServer(
        id: base.id,
        createdDate: createdDate,
        location: location,
        version: version,
        name: base.name,
        host: WSServer.fromJson(json[_HOST_FIELD]),
        local: json[_LOCAL_FIELD],
        price: base.price,
        description: base.description,
        visible: json[_VISIBLE_FIELD],
        availableOnSignup: json[_AVAILABLE_ON_SIGNUP_FIELD],
        backgroundUrl: base.backgroundUrl);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    if (location != null) {
      result[_LOCATION_FIELD] = location!.toJson();
    }
    if (version != null) {
      result[_VERSION_FIELD] = version;
    }
    result[_HOST_FIELD] = host.toJson();
    result[_LOCAL_FIELD] = local;
    result[_VISIBLE_FIELD] = visible;
    result[_AVAILABLE_ON_SIGNUP_FIELD] = availableOnSignup;
    return result;
  }
}
