import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/utils.dart';

class ProviderStatus {
  static const _NOT_ACTIVE_CONSTANT = 0;
  static const _ACTIVE_CONSTANT = 1;

  final int _value;

  const ProviderStatus._(this._value);

  int compareTo(ProviderStatus other) {
    return _value.compareTo(other._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NOT_ACTIVE_CONSTANT) {
      return 'NOT ACTIVE';
    }

    assert(_value == _ACTIVE_CONSTANT);
    return 'ACTIVE';
  }

  factory ProviderStatus.fromInt(int status) {
    if (status == _NOT_ACTIVE_CONSTANT) {
      return ProviderStatus.NOT_ACTIVE;
    } else if (status == _ACTIVE_CONSTANT) {
      return ProviderStatus.ACTIVE;
    }

    throw 'Unknown provider status: $status';
  }

  static List<ProviderStatus> get values => [NOT_ACTIVE, ACTIVE];

  static const ProviderStatus NOT_ACTIVE = ProviderStatus._(_NOT_ACTIVE_CONSTANT);
  static const ProviderStatus ACTIVE = ProviderStatus._(_ACTIVE_CONSTANT);
}

class ProviderType {
  static const int _ADMIN_CONSTANT = 0;
  static const int _SUPER_RESELLER_CONSTANT = 1;
  static const int _RESELLER_CONSTANT = 2;
  static const int _CHANNEL_ADMIN_CONSTANT = 3;
  static const int _ANALYTICS_CONSTANT = 4;

  final int _value;

  const ProviderType._(this._value);

  int compareTo(ProviderType other) {
    return _value.compareTo(other._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _ADMIN_CONSTANT) {
      return 'ADMIN';
    } else if (_value == _SUPER_RESELLER_CONSTANT) {
      return 'SUPER RESELLER';
    } else if (_value == _RESELLER_CONSTANT) {
      return 'RESELLER';
    } else if (_value == _CHANNEL_ADMIN_CONSTANT) {
      return 'CHANNEL ADMIN';
    }

    assert(_value == _ANALYTICS_CONSTANT);
    return 'ANALYTICS';
  }

  factory ProviderType.fromInt(int type) {
    if (type == _ADMIN_CONSTANT) {
      return ProviderType.ADMIN;
    } else if (type == _SUPER_RESELLER_CONSTANT) {
      return ProviderType.SUPER_RESELLER;
    } else if (type == _RESELLER_CONSTANT) {
      return ProviderType.RESELLER;
    } else if (type == _CHANNEL_ADMIN_CONSTANT) {
      return ProviderType.CHANNEL_ADMIN;
    } else if (type == _ANALYTICS_CONSTANT) {
      return ProviderType.ANALYTICS;
    }

    throw 'Unknown provider type: $type';
  }

  static List<ProviderType> get values =>
      [ADMIN, SUPER_RESELLER, RESELLER, CHANNEL_ADMIN, ANALYTICS];

  static const ProviderType ADMIN = ProviderType._(_ADMIN_CONSTANT);
  static const ProviderType SUPER_RESELLER = ProviderType._(_SUPER_RESELLER_CONSTANT);
  static const ProviderType RESELLER = ProviderType._(_RESELLER_CONSTANT);
  static const ProviderType CHANNEL_ADMIN = ProviderType._(_CHANNEL_ADMIN_CONSTANT);
  static const ProviderType ANALYTICS = ProviderType._(_ANALYTICS_CONSTANT);
}

class ProviderAdd {
  static const _ID_FIELD = 'id';
  static const EMAIL_FIELD = 'email';
  static const FIRST_NAME_FIELD = 'first_name';
  static const LAST_NAME_FIELD = 'last_name';
  static const _PASSWORD_FIELD = 'password';
  static const _CREATED_DATE_FIELD = 'created_date';
  static const TYPE_FIELD = 'type';
  static const STATUS_FIELD = 'status';
  static const LANGUAGE_FIELD = 'language';
  static const COUNTRY_FIELD = 'country';
  static const CREDITS_FIELD = 'credits';
  static const _OWNER_FIELD = 'owner';

  static const MIN_NAME = 2;
  static const MAX_NAME = 64;

  final String? id;
  final UtcTimeMsec? createdDate;
  String email;
  String password;
  String firstName;
  String lastName;
  ProviderType type;
  ProviderStatus status;
  String? language;
  String? country;
  String? owner;
  int credits;

  ProviderAdd(
      {this.id,
      required this.email,
      required this.password,
      required this.firstName,
      required this.lastName,
      this.createdDate,
      required this.status,
      required this.type,
      required this.credits,
      required this.language,
      required this.country,
      required this.owner});

  ProviderAdd.create(
      {required this.email,
      required this.password,
      required this.firstName,
      required this.lastName,
      required this.createdDate,
      required this.type,
      required this.status,
      required this.credits,
      this.language,
      this.country,
      required this.owner})
      : id = null;

  ProviderAdd.createDefault({this.type = ProviderType.ADMIN})
      : id = null,
        email = '',
        firstName = '',
        lastName = '',
        createdDate = null,
        owner = null,
        password = '',
        status = ProviderStatus.ACTIVE,
        credits = Credits.DEFAULT;

  ProviderAdd.createChannelAdmin()
      : id = null,
        type = ProviderType.CHANNEL_ADMIN,
        email = '',
        firstName = '',
        lastName = '',
        createdDate = null,
        owner = null,
        password = '',
        status = ProviderStatus.ACTIVE,
        credits = 0;

  ProviderAdd.createAnalyticsAdmin()
      : id = null,
        type = ProviderType.ANALYTICS,
        email = '',
        firstName = '',
        lastName = '',
        createdDate = null,
        owner = null,
        password = '',
        status = ProviderStatus.ACTIVE,
        credits = 0;

  ProviderAdd copy() {
    return ProviderAdd(
        id: id,
        email: email,
        firstName: firstName,
        password: password,
        lastName: lastName,
        createdDate: createdDate,
        language: language,
        country: country,
        owner: owner,
        type: type,
        status: status,
        credits: credits);
  }

  bool isAdmin() {
    return type == ProviderType.ADMIN;
  }

  bool isReseller() {
    return type == ProviderType.RESELLER;
  }

  bool isSuperReseller() {
    return type == ProviderType.SUPER_RESELLER;
  }

  bool isChannelAdmin() {
    return type == ProviderType.CHANNEL_ADMIN;
  }

  bool isAnalytics() {
    return type == ProviderType.ANALYTICS;
  }

  bool isValid() {
    if (language == null || country == null) {
      return false;
    }

    final bool base = email.isNotEmpty &&
        firstName.isNotEmpty &&
        lastName.isNotEmpty &&
        language!.isNotEmpty &&
        country!.isNotEmpty &&
        credits.isValidCredits();

    if (id == null) {
      return base && password.isNotEmpty;
    }

    if (createdDate == null) {
      return false;
    }
    return base && createdDate! >= 0;
  }

  factory ProviderAdd.fromJson(Map<String, dynamic> json) {
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    UtcTimeMsec? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    String? owner;
    if (json.containsKey(_OWNER_FIELD)) {
      owner = json[_OWNER_FIELD];
    }
    return ProviderAdd(
        id: id,
        email: json[EMAIL_FIELD],
        firstName: json[FIRST_NAME_FIELD],
        lastName: json[LAST_NAME_FIELD],
        createdDate: createdDate,
        type: ProviderType.fromInt(json[TYPE_FIELD]),
        status: ProviderStatus.fromInt(json[STATUS_FIELD]),
        credits: json[CREDITS_FIELD],
        language: json[LANGUAGE_FIELD],
        owner: owner,
        country: json[COUNTRY_FIELD],
        password: json[_PASSWORD_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      EMAIL_FIELD: email,
      FIRST_NAME_FIELD: firstName,
      LAST_NAME_FIELD: lastName,
      TYPE_FIELD: type.toInt(),
      STATUS_FIELD: status.toInt(),
      CREDITS_FIELD: credits,
      LANGUAGE_FIELD: language,
      COUNTRY_FIELD: country,
      _PASSWORD_FIELD: password
    };
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    if (owner != null) {
      result[_OWNER_FIELD] = owner;
    }
    return result;
  }
}

class Provider extends ProviderAdd {
  static const CREDITS_REMAINING_FIELD = 'credits_remaining';
  int? creditsRemaining;

  Provider(
      {String? id,
      required String email,
      required String password,
      required String firstName,
      required String lastName,
      UtcTimeMsec? createdDate,
      required ProviderStatus status,
      required ProviderType type,
      required int credits,
      required this.creditsRemaining,
      required String? language,
      required String? country,
      required String? owner})
      : super(
            id: id,
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            createdDate: createdDate,
            type: type,
            status: status,
            credits: credits,
            language: language,
            country: country,
            owner: owner);

  Provider.create(
      {required String email,
      required String password,
      required String firstName,
      required String lastName,
      required UtcTimeMsec createdDate,
      required ProviderType type,
      required ProviderStatus status,
      required int credits,
      this.creditsRemaining,
      required String? language,
      required String? country,
      required String? owner})
      : super.create(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            createdDate: createdDate,
            type: type,
            status: status,
            credits: credits,
            language: language,
            country: country,
            owner: owner);

  Provider.createDefault({ProviderType type = ProviderType.ADMIN})
      : creditsRemaining = 0,
        super.createDefault(type: type);

  @override
  Provider copy() {
    return Provider(
        id: id,
        email: email,
        firstName: firstName,
        password: password,
        lastName: lastName,
        createdDate: createdDate,
        language: language,
        country: country,
        owner: owner,
        type: type,
        status: status,
        credits: credits,
        creditsRemaining: creditsRemaining);
  }

  factory Provider.fromJson(Map<String, dynamic> json) {
    final prov = ProviderAdd.fromJson(json);
    int? creditsRemaining;
    if (json.containsKey(CREDITS_REMAINING_FIELD)) {
      creditsRemaining = json[CREDITS_REMAINING_FIELD];
    }
    return Provider(
        id: prov.id,
        email: prov.email,
        firstName: prov.firstName,
        lastName: prov.lastName,
        createdDate: prov.createdDate,
        type: prov.type,
        status: prov.status,
        credits: prov.credits,
        creditsRemaining: creditsRemaining,
        language: prov.language,
        country: prov.country,
        owner: prov.owner,
        password: prov.password);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (createdDate != null) {
      result[CREDITS_REMAINING_FIELD] = creditsRemaining;
    }
    return result;
  }
}
