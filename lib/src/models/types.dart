const int _MAX_INTEGER_NUMBER = 1000000;

extension Credits on int {
  static const int MIN = 0;
  static const int MAX = _MAX_INTEGER_NUMBER;
  static const int DEFAULT = 100;

  bool isValidCredits() {
    return this >= MIN && this <= MAX;
  }
}

extension DevicesCount on int {
  static const int MAX = _MAX_INTEGER_NUMBER;
  static const int MIN = 0;
  static const DEFAULT = 10;

  bool isValidDevicesCount() {
    return this >= MIN && this <= MAX;
  }
}
