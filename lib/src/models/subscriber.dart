import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/profile.dart';
import 'package:fastotv_dart/utils.dart';

class Subscriber extends SubscriberAdd {
  static const _ID_FIELD = 'id';
  static const _DEVICES_FIELD = 'devices';

  final String? id;
  List<String> devices;

  int get devicesCount {
    return devices.length;
  }

  Subscriber(
      {this.id,
      required String email,
      required String password,
      required String firstName,
      required String lastName,
      UtcTimeMsec? createdDate,
      required UtcTimeMsec expDate,
      required SubscriberStatus status,
      required int maxDevicesCount,
      required String language,
      required String country,
      required String? owner,
      required List<String> servers,
      required this.devices})
      : super(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            expDate: expDate,
            createdDate: createdDate,
            status: status,
            maxDevicesCount: maxDevicesCount,
            language: language,
            country: country,
            owner: owner,
            servers: servers);

  Subscriber.create(
      {this.id,
      required String email,
      required String password,
      required String firstName,
      required String lastName,
      required UtcTimeMsec expDate,
      required SubscriberStatus status,
      required int maxDevicesCount,
      required String language,
      required String country,
      required String? owner,
      required List<String> servers,
      required this.devices})
      : super.create(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            expDate: expDate,
            status: status,
            maxDevicesCount: maxDevicesCount,
            language: language,
            country: country,
            owner: owner,
            servers: servers);

  Subscriber.createDefault()
      : id = null,
        devices = [],
        super.createDefault(DevicesCount.DEFAULT);

  @override
  Subscriber copy() {
    return Subscriber(
        id: id,
        email: email,
        firstName: firstName,
        password: password,
        lastName: lastName,
        createdDate: createdDate,
        expDate: expDate,
        status: status,
        language: language,
        country: country,
        owner: owner,
        maxDevicesCount: maxDevicesCount,
        servers: servers,
        devices: devices);
  }

  @override
  bool isValid() {
    final bool fields = email.isNotEmpty &&
        SubscriberSignUp.isValidName(firstName) &&
        SubscriberSignUp.isValidName(lastName) &&
        expDate >= 0 &&
        language.isNotEmpty &&
        country.isNotEmpty;

    if (id == null) {
      return fields && password.isNotEmpty;
    }

    if (createdDate == null) {
      return false;
    }
    return fields && createdDate! >= 0;
  }

  // password field not exists in json
  factory Subscriber.fromJson(Map<String, dynamic> json) {
    final profile = SubscriberAdd.fromJson(json);
    final List<String> devices = json[_DEVICES_FIELD].cast<String>();
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    return Subscriber(
        id: id,
        email: profile.email,
        password: profile.password,
        firstName: profile.firstName,
        lastName: profile.lastName,
        createdDate: profile.createdDate,
        expDate: profile.expDate,
        status: profile.status,
        maxDevicesCount: profile.maxDevicesCount,
        language: profile.language,
        country: profile.country,
        owner: profile.owner,
        servers: profile.servers,
        devices: devices);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    result[_DEVICES_FIELD] = devices;
    return result;
  }
}
